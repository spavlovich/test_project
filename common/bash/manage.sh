#!/bin/bash
set -a
. ./.env
set +a

# Create network if not exist
add_docker_network


function copy_python_packages() {
    if [[ $1 = 'test' ]]; then
        PIP_PACKAGES="./docker/python/config/tests-requirements.txt"
    else
        PIP_PACKAGES="./docker/python/config/requirements.txt"
    fi
    PACKAGES=()
    while IFS="" read -r item || [ -n "$item" ]
    do
        API_ENDPOINT=`echo ${item} | sed -r "s/\/python-packages\/([^ .]*)( #)?([^#.*])?/\1,\3/"`
        if [[ $API_ENDPOINT != ${item} ]]; then
            PACKAGES+=(${API_ENDPOINT})
        fi
    done < ${PIP_PACKAGES}

    mkdir -p ./docker/python/packages/
    rm -rf ./docker/python/packages/*
    if [ -n "$PACKAGES" ]; then
        BASE_URL=`echo ${CI_REPOSITORY_URL} | sed "s;\/*${CI_PROJECT_PATH}.*;;"`
        if [ -z "$BASE_URL" ]
        then
            BASE_URL=ssh://git@git.some.ru:8122
        fi

        TMP_FOLDER=~/.__git_tmp__
        mkdir -p ${TMP_FOLDER}
        rm -rf ${TMP_FOLDER}/*

        for item in ${PACKAGES[*]}
        do
            IFS=',' read -r -a item_array <<< $item
            repo=${item_array[0]}
            branch=${item_array[1]:-master}

            git clone -b $branch ${BASE_URL}/back/${repo}.git ${TMP_FOLDER}/${repo}
            API_PATH=${TMP_FOLDER}/${repo}/api
            PACKAGE_NAME=${repo}
            PACKAGE_NAME=${repo//-/_}
            echo '{"packageName" : "'${PACKAGE_NAME}'"}' >  ${API_PATH}/python-config.json
            docker run -i -v ${API_PATH}:/swagger-generated jimschubert/swagger-codegen-cli generate -i /swagger-generated/client.yaml -l python -c /swagger-generated/python-config.json -o /swagger-generated/python
            cp -r  ${API_PATH}/python ./docker/python/packages/${repo}
            cp ${API_PATH}/client.yaml ./docker/python/packages/${repo}/${PACKAGE_NAME}/client.yaml
            
            YAML=./docker/python/packages/${repo}/${PACKAGE_NAME}/yaml.py
            echo 'import yaml as _yaml' > ${YAML}
            echo 'import yamlloader' >> ${YAML}
            echo 'import os' >> ${YAML}
            echo 'with open("%s/client.yaml" % os.path.dirname(os.path.abspath(__file__)), "r") as stream:' >> ${YAML}
            echo '    yaml = _yaml.load(stream, Loader=yamlloader.ordereddict.CLoader)' >> ${YAML}

            echo "from ${PACKAGE_NAME}.yaml import yaml" >> ./docker/python/packages/${repo}/${PACKAGE_NAME}/__init__.py
            echo "include ${PACKAGE_NAME}/client.yaml" > ./docker/python/packages/${repo}/MANIFEST.in

            docker run -i -v ${API_PATH}:/swagger-generated jimschubert/swagger-codegen-cli rm -rf /swagger-generated/python
        done
        rm -rf ${TMP_FOLDER}
    fi
}



function wait_db()
{
    printf "Waiting DB"
    while true; do
        if [[ $(docker exec -i ${DB_CONTAINER} bash -c "(</dev/tcp/${DB_CONTAINER}/${POSTGRES_PORT}) 2>/dev/null" && echo 1 || echo 0) -eq 1 ]]; then 
            printf ". Connected.\n"
            break
        else 
            printf "."
        fi 
        sleep 1
    done
}



#--- Help ---
function manage_actions {
    echo $actions
}
function manage_usage {
    # Print out usage message.
    for act in $actions; do
        funcname=man_${act}
        if [ "$(type -t $funcname)" != 'function' ]; then
            echo "    ${act}"
        else
            $funcname
        fi
    done
}


#--- Command-line interface ---
action=
while getopts h arg; do
    case $arg in
        h|?)
            action=usage
            ;;
    esac
done
shift $(($OPTIND-1))



_actions="$actions actions"

for act in $_actions; do
    if [[ $act == $1 ]]; then
        action=$act
        break
    fi
done

args=""
for i in ${*:2}; do
    args="$args $i"
done

if [[ -n $action ]]; then
    manage_$action
else
    [[ $# -gt 0 ]] && echo "Unknown option/action: $1." || echo "Action not supplied."
    manage_usage
fi
