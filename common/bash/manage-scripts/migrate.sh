function man_migrate() {
    cat <<END
    migrate
        manage.py migrate
END
}
function manage_migrate() {
    docker exec -i ${PYTHON_CONTAINER} ./manage.py migrate
}