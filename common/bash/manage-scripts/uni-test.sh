function man_uni-test() {
    cat <<END
    uni-test
        unit testing
END
}
function manage_uni-test() {
    copy_python_packages 'test'
    APP_PATH=$PWD/app
    PACKAGES_PATH=$PWD/docker/python/packages
    REQUIREMENTS_PATH=$PWD/docker/python/config/tests-requirements.txt
    docker run -i\
        -v $APP_PATH:/app\
        -v $PACKAGES_PATH:/python-packages\
        -v $REQUIREMENTS_PATH:/requirements.txt\
        python:3.5 bash -c "pip3 install -r /requirements.txt --find-links /python-packages; python3 -B -m unittest discover --start-directory /app/tests -v"
}