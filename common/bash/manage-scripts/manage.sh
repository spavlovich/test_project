function man_manage() {
    cat <<END
    manage [arg1 arg2 arg...]
        manage.py [arg1 arg2 arg...]
END
}
function manage_manage() {
    docker exec -it ${PYTHON_CONTAINER} ./manage.py $args
}