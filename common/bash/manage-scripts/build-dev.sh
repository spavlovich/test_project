function man_build-dev() {
    cat <<END
    build-dev [-recreate]
        Deploy development environment
END
}
function manage_build-dev() {
    copy_python_packages
    docker-compose -f docker-compose.yml -f docker-compose.dev.yml config > docker-compose.override.yml
    docker-compose stop
    docker-compose rm -f
    if [ ! -z "$args" ] && [ $args == "-recreate" ]; then
        manage_database-recreate
    fi
    docker-compose up -d --build
    wait_db
    manage_makemigrations
    manage_migrate
    if [ ! -z "$args" ] && [ $args == "-recreate" ]; then
        manage_superuser-create
    fi
    echo '--> Dev Deployed'
}