function man_collectstatic() {
    cat <<END
    collectstatic
        manage.py collectstatic --noinput
END
}
function manage_collectstatic() {
    docker exec -i ${PYTHON_CONTAINER} ./manage.py collectstatic --noinput
}