function man_makemigrations() {
    cat <<END
    makemigrations
        manage.py makemigrations
END
}
function manage_makemigrations() {
    docker exec -i ${PYTHON_CONTAINER} ./manage.py makemigrations
}