function add_docker_network() {
    if [[ $(docker network ls | grep 'cle-back' | wc -l) -eq 0 ]]; then 
        docker network create --driver bridge --subnet 10.57.0.0/24 --ip-range 10.57.0.0/24 cle-back
    fi
}