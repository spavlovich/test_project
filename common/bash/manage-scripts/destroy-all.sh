function man_destroy-all() {
    cat <<END
    destroy-all
        Container destroy all containers
END
}
function manage_destroy-all() {
    docker stop $(docker ps -a -q)
    docker rm $(docker ps -a -q)
    docker rmi -f $(docker images -q)
}