function man_build-stage() {
    cat <<END
    build-stage
        Deploy stage environment
END
}
function manage_build-stage() {
    copy_python_packages
    docker-compose -f docker-compose.yml -f docker-compose.stage.yml config > docker-compose.override.yml
    docker-compose stop
    docker-compose rm -f
    docker-compose up -d --build
    wait_db
    manage_migrate
    manage_collectstatic
    echo '--> Stage Deployed'
}