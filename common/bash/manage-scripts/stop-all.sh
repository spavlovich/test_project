function man_stop-all() {
    cat <<END
    stop-all
        Container stop all containers
END
}
function manage_stop-all() {
    docker stop $(docker ps -a -q)
    docker rm $(docker ps -a -q)
}