function man_superuser-create() {
    cat <<END
    superuser-create
        Create django super user
END
}
function manage_superuser-create() {
	docker exec -i ${PYTHON_CONTAINER} bash -c "./manage.py shell < ./common/django/scripts/create_admin_user.py"
}
