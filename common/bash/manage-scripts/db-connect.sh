function man_db-connect() {
    cat <<END
    db-connect
        Connect to the database
END
}
function manage_db-connect() {
    docker exec -it ${DB_CONTAINER} bash -c 'PGPASSWORD=${POSTGRES_PASSWORD} psql -h 127.0.0.1 -U ${POSTGRES_USER} ${POSTGRES_DB}'
}