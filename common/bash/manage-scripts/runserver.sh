function man_runserver() {
    cat <<END
    runserver
        manage.py runserver 0.0.0.0:8000
END
}
function manage_runserver() {
    docker exec -it ${PYTHON_CONTAINER} ./manage.py runserver 0.0.0.0:80
}