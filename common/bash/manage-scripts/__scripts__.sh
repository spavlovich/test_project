#!/bin/bash
set -a
. ./.env
set +a
. ./common/bash/manage-scripts/build-dev.sh
. ./common/bash/manage-scripts/build-stage.sh
. ./common/bash/manage-scripts/stop-all.sh
. ./common/bash/manage-scripts/destroy-all.sh
. ./common/bash/manage-scripts/collectstatic.sh
. ./common/bash/manage-scripts/makemigrations.sh
. ./common/bash/manage-scripts/migrate.sh
. ./common/bash/manage-scripts/runserver.sh
. ./common/bash/manage-scripts/manage.sh
. ./common/bash/manage-scripts/superuser-create.sh
. ./common/bash/manage-scripts/uni-test.sh
. ./common/bash/manage-scripts/database-recreate.sh
. ./common/bash/manage-scripts/db-connect.sh
. ./common/bash/manage-scripts/add_docker_network.sh
