from drf_yasg.inspectors import SwaggerAutoSchema as DefaultSwaggerAutoSchema
from drf_yasg.utils import merge_params
from drf_yasg import openapi

class SwaggerAutoSchema(DefaultSwaggerAutoSchema):
    def get_query_parameters(self):
        """Return the query parameters accepted by this view.

        :rtype: list[openapi.Parameter]
        """
        natural_parameters = self.get_filter_parameters() + self.get_pagination_parameters()

        query_serializer = self.get_query_serializer()
        serializer_parameters = []
        if query_serializer is not None:
            serializer_parameters = self.serializer_to_parameters(query_serializer, in_=openapi.IN_QUERY)

        return merge_params(natural_parameters, serializer_parameters)