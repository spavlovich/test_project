from rest_framework import serializers
from django.conf import settings


class ThumbSerializer(serializers.Serializer):
    read_only=True
    required=False
    
    def __init__(self, read_only=None, required=None, *args, **kwargs):
        super().__init__(read_only=read_only, required=required, *args, **kwargs)
        
        if read_only == None:
            self.read_only = True

        if required == None:
            self.required = False

    def get_fields(self):
        fields = {}
        for key, value in settings.THUMB_SIZES.items():
            fields[key] = serializers.SerializerMethodField(label=value, method_name=self.thumb_url(key))
        return fields

    def thumb_url(self, thumb_key):
        function_name = '__thumb_%s' % thumb_key
        def __thumb(item):
            if not item:
                return None
            return item.thumb_url_template % thumb_key

        setattr(self, function_name, __thumb)
        return function_name