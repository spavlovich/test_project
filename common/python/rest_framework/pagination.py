from rest_framework import pagination
from rest_framework.settings import api_settings
from rest_framework.exceptions import ParseError
from rest_framework.response import Response
from collections import OrderedDict

def _positive_int(integer_string, strict=False, cutoff=None):
    """
    Cast a string to a strictly positive integer.
    """
    ret = int(integer_string)
    if ret < 0 or (ret == 0 and strict):
        raise ValueError()

    cutoff = max(0, cutoff)
    if cutoff != None and cutoff < ret:
        raise ValueError()
    return ret


class LimitOffsetPagination(pagination.LimitOffsetPagination):
    max_limit = api_settings.MAX_LIMIT
    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('count', self.count),
            ('results', data)
        ]))

    def get_limit(self, request):
        if self.limit_query_param and request.query_params.get(self.limit_query_param) != None:
            try:
                return _positive_int(
                    request.query_params[self.limit_query_param],
                    strict=True,
                    cutoff=self.max_limit
                )
            except (KeyError, ValueError):
                raise ParseError('limit is invalid')

        return self.default_limit

    def get_offset(self, request):
        if request.query_params.get(self.offset_query_param) != None:
            try:
                return _positive_int(
                    request.query_params[self.offset_query_param],
                    strict=False,
                    cutoff=self.count - 1
                )
            except (KeyError, ValueError):
                raise ParseError('offset is invalid')

        return 0