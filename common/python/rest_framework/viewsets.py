from rest_framework import viewsets
from rest_framework.settings import api_settings
from rest_framework import serializers


class ApiViewSet(viewsets.ViewSet):
    serializer_classes = None
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS

    def get_serializer(self, *args, **kwargs):
        """
        Return the serializer instance that should be used for validating and
        deserializing input, and for serializing output.
        """
        serializer_class = self.get_serializer_class()
        if serializer_class:
            kwargs['context'] = self.get_serializer_context()
            return serializer_class(*args, **kwargs)
        else:
            return None

    def get_serializer_class(self):
        serializer_classes = self.serializer_classes or {}
        return serializer_classes.get(self.action)

    def get_serializer_context(self):
        """
        Extra context provided to the serializer class.
        """
        return {
            'request': self.request,
            'format': self.format_kwarg,
            'view': self
        }

    @property
    def paginator(self):
        """
        The paginator instance associated with the view, or `None`.
        """
        if not hasattr(self, '_paginator'):
            if self.pagination_class is None:
                self._paginator = None
            else:
                self._paginator = self.pagination_class()
                # self._paginator = None
        return self._paginator

    def get_paginated_response(self, data):
        """
        Return a paginated style `Response` object for the given output data.
        """
        assert self.paginator is not None

        serializer = self.get_serializer(data=data['results'], many=True)
        self.paginator.count = data['count']
        serializer.is_valid(raise_exception=True)

        return self.paginator.get_paginated_response(serializer.data)