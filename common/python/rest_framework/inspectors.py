from drf_yasg import inspectors

class DjangoRestResponsePagination(inspectors.DjangoRestResponsePagination):
    def get_paginated_response(self, paginator, response_schema):
        paged_schema = super().get_paginated_response(paginator, response_schema)
        del paged_schema.properties['next']
        del paged_schema.properties['previous']
        return paged_schema