from .serializer_maker import SerializerMaker

class ApiMethodSerializer:
    def __init__(self, module_name, data, definition_serializers):
        self._module_name = module_name
        self._data = data
        self._definition_serializers = definition_serializers
        self._response = self._get_response_serializers()
        self._body = self._get_serializer_by_param_type('body')
        self._path = self._get_serializer_by_param_type('path')
        self._query = self._get_serializer_by_param_type('query')

    @property
    def response(self):
        return self._response

    @property
    def body(self):
        return self._body

    @property
    def path(self):
        return self._path

    @property
    def query(self):
        return self._query

    @property
    def definition_serializers(self):
        return self._definition_serializers
    


    def _get_serializer_by_param_type(self, param_type):
        params = list(filter(lambda d: d['in'] in param_type, self._data['parameters']))
        if not params:
            return None

        serializer_name = "%s_%s_serializer" % (self._data['operationId'], param_type)
        maker = SerializerMaker(self.definition_serializers)
        serializer = maker.by_params(serializer_name, params)
        return serializer


    def _get_response_serializers(self):
        responses = {}
        for status_code, response in self._data['responses'].items():
            status_code = int(status_code)
            schema = response.get('schema')
            if schema:
                serializer_name = "%s_%s_serializer" % (self._data['operationId'], status_code)
                maker = SerializerMaker(self.definition_serializers)
                serializer = maker.by_definition(serializer_name, schema, without_read_only=True)
                if serializer:
                    responses[status_code] = serializer
        return responses
    
    
    
    

class Serializers:
    def __init__(self, definitions):
        self._definitions = definitions
        self._set_definitions()

    def _set_definitions(self):
        for definition_name, info in self._definitions.items():
            maker = SerializerMaker(self)
            serializer = maker.by_definition(definition_name, info)
            setattr(self, definition_name, serializer)

            definition_without_read_only_name = "%sWithoutReadOnly" % definition_name
            serializer = maker.by_definition(definition_without_read_only_name, info, without_read_only=True)
            setattr(self, definition_without_read_only_name, serializer)