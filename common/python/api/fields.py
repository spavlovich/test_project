from rest_framework.fields import (
    CharField, ListField
)


class ApiCharField(CharField):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.allow_blank = kwargs.pop('allow_blank', True)


class ApiCharacterSeparatedField(ListField):

    def __init__(self, *args, **kwargs):
        self.separator = kwargs.pop('separator', ',')
        super().__init__(*args, **kwargs)

    def to_internal_value(self, data):
        data = data.split(self.separator)
        data = super().to_internal_value(data)
        return self.separator.join([str(i) for i in data])
