from collections import OrderedDict
from rest_framework import serializers
from rest_framework.fields import (  # NOQA # isort:skip
    BooleanField, CharField, ChoiceField, DateField, DateTimeField, DecimalField,
    DictField, DurationField, EmailField, Field, FileField, FilePathField, FloatField,
    HiddenField, HStoreField, IPAddressField, ImageField, IntegerField, JSONField,
    ListField, ModelField, MultipleChoiceField, NullBooleanField, ReadOnlyField,
    RegexField, SerializerMethodField, SlugField, TimeField, URLField, UUIDField,
)
from rest_framework.settings import api_settings
from .fields import (
    ApiCharField, ApiCharacterSeparatedField
)

class SerializerMaker:
    FIELD_MAPPING = {
        'string': ApiCharField,
        'integer': IntegerField,
        'number': FloatField,
        'array': ListField,
        'date': DateField,
        'date-time': DateTimeField,
        'uri': URLField,
        'boolean': BooleanField,
        'email': EmailField
    }

    PROPERTY_MAPPING = {
        'title': 'label',
        'description': 'help_text',
        'minLength': 'min_length',
        'maxLength': 'max_length',
        'maximum': 'max_value',
        'minimum': 'min_value',
        'x-nullable': 'allow_null',
        'readOnly': 'read_only',
        'items': 'child',
        'format': None,
        'type': None,
        'pattern': 'regex',
        'required': 'required',
        'enum': 'choices',
        'uniqueItems': None,
        'minItems': 'min_length',
        'maxItems': 'max_length',
        'default': 'default',
        'separator': 'separator',
        '$ref': None
    }

    def __init__(self, definitions):
        self._definitions = definitions

    @property
    def definitions(self):
        return self._definitions

    def empty_serialize(self, name):
        class serializer_class(serializers.Serializer):
            pass
        serializer = serializer_class
        serializer.__name__ = name
        return serializer

    def _make_params(self, source_params, without_read_only):
        field_type = source_params.get('format') or source_params.get('type')
        params = {}
        for key, val in source_params.items():
            new_key = self.PROPERTY_MAPPING[key]
            if new_key:
                params[new_key] = val

        if without_read_only and params.get('read_only'):
            del params['read_only']


        if params.get('child'):
            ref = params['child'].get('$ref')
            if ref:
                child_definition = params['child']['$ref'].split('/')[-1]
                if without_read_only:
                    child_definition += "WithoutReadOnly"
                params['child'] = getattr(self.definitions, child_definition)()
            else:
                params['child'] = self._make_field(params['child'], without_read_only)

        return params


    def _make_field(self, property_data, without_read_only=False):
        if '$ref' in property_data:
            child_definition = property_data['$ref'].split('/')[-1]
            if without_read_only:
                child_definition += "WithoutReadOnly"
            property_data['x-nullable'] = True
            return getattr(self.definitions, child_definition)(
                **self._make_params(property_data, without_read_only)
            )
        else:
            field_type = property_data.get('format') or property_data.get('type')
            field = self.FIELD_MAPPING.get(field_type)
            

            if not field:
                raise Exception("Swagger to Serializer: type `%s` is undefined" % field_type)


            if property_data.get('pattern'):
                field = RegexField
            elif property_data.get('enum'):
                field = ChoiceField
            elif property_data.get('separator'):
                field = ApiCharacterSeparatedField
                property_data['items'] = {
                    'type': field_type
                }

            params = self._make_params(property_data, without_read_only)
            return field(**params)

    def by_params(self, name, params_data, without_read_only=False):
        if len(params_data) and params_data[0]['in'] == 'body':
            child_definition = params_data[0]['schema']['$ref'].split('/')[-1]
            if without_read_only:
                child_definition += "WithoutReadOnly"
            return getattr(self.definitions, child_definition)

        serializer = self.empty_serialize(name)
        fields = OrderedDict()
        for param_data in params_data:
            property_data = OrderedDict(param_data)
            property_name = property_data.pop('name')
            where = property_data.pop('in')
            if where == 'query':
                if property_name == 'limit':
                    property_data['maximum'] = api_settings.MAX_LIMIT
                    property_data['minimum'] = 1
                elif property_name == 'offset':
                    property_data['minimum'] = 0
                elif property_name.endswith('__in'):
                    property_data['separator'] = ','

            fields[property_name] = self._make_field(property_data, without_read_only)
        serializer._declared_fields = fields

        return serializer


    def by_definition(self, name, data, without_read_only=False):
        serializer = self.empty_serialize(name)
        if data.get('type') == 'array':
            data = data['items']

        if data.get('$ref'):
            child_definition = data['$ref'].split('/')[-1]
            if without_read_only:
                child_definition += "WithoutReadOnly"
            return getattr(self.definitions, child_definition)

        if not data.get('properties'):
            return None

        fields = OrderedDict()
        for property_name, _property_data in data['properties'].items():
            property_data = OrderedDict(_property_data)
            property_data['required'] = bool(property_name in data.get('required', []))
            fields[property_name] = self._make_field(property_data, without_read_only)
        serializer._declared_fields = fields
        return serializer
    