from rest_framework.response import Response
from .serializer import Serializers, ApiMethodSerializer
from .exceptions import ApiException
import json
from collections import OrderedDict


class ApiMethodResponse:

    def __init__(self, data, serializer, status_code):
        self._data_source = data
        self._serializer = serializer
        self._status_code = status_code

    @property
    def data_source(self):
        return self._data_source

    @property
    def data(self):
        if not self.serializer or not self.data_source:
            return self.data_source

        serializer = self.serializer(
            data=self.data_source, many=isinstance(self.data_source, list))
        serializer.is_valid(raise_exception=True)
        return serializer.validated_data.copy()

    @property
    def results(self):
        data = self.data
        if isinstance(data, dict) and data.get('results'):
            return data.get('results')
        return data
    

    def data_by_key(self, key):
        results = self.results
        res = OrderedDict()
        for i in results:
            res[i[key]] = i
        return res

    @property
    def data_by_id(self):
        return self.data_by_key('id')

    @property
    def data_to_response(self):
        if not self.serializer or not self.data_source:
            return self.data_source

        serializer = self.serializer(
            instance=self.data_source, many=isinstance(self.data_source, list))
        return serializer.data

    @property
    def status_code(self):
        return self._status_code

    @property
    def serializer(self):
        return self._serializer

    def response(self):
        return Response(
            data=self.data_to_response,
            status=self.status_code
        )


class ApiMethod:

    def __init__(self, module_name, method, data, definition_serializers):
        self._module_name = module_name
        self._method = method
        self._data = data
        self._definition_serializers = definition_serializers
        self._serializers = ApiMethodSerializer(
            module_name, data, self.definition_serializers)

    @property
    def method(self):
        return self._method

    @property
    def serializers(self):
        return self._serializers

    @property
    def definition_serializers(self):
        return self._definition_serializers

    def response_serializer(self, code):
        return self.serializers.response.get(code)

    def get_query_data(self, request=None, **kwargs):
        if request:
            query = {**request.GET.dict()}
        else:
            query = {}
        query.update(kwargs)
        if self.serializers.query:
            serializer = self.serializers.query(data=query)
            serializer.is_valid(raise_exception=True)
            return serializer.validated_data
        else:
            return query

    def get_body_data(self, request=None, **kwargs):
        if request:
            query = {**request.data}
        else:
            query = {}
        query.update(kwargs.get('data', {}))
        if self.serializers.body:
            partial = self.method.__name__.endswith(
                '_partial_update_with_http_info')
            serializer = self.serializers.body(data=query, partial=partial)
            serializer.is_valid(raise_exception=True)
            return serializer.validated_data
        else:
            return query

    def get_path_data(self, request=None, **kwargs):
        if request:
            query = {**request.resolver_match.kwargs}
        else:
            query = {}
        query.update(kwargs)
        if query.get('pk'):
            query['id'] = query.pop('pk')

        if self.serializers.path:
            serializer = self.serializers.path(data=query)
            serializer.is_valid(raise_exception=True)
            return serializer.validated_data
        else:
            return query

    def call(self, request=None, **kwargs):
        data = kwargs.get('data')
        if not data and request:
            data = request.data

        query_data = self.get_query_data(request=request, **kwargs)
        body_data = self.get_body_data(request=request, **kwargs)
        path_data = self.get_path_data(request=request, **kwargs)

        params = {}
        params.update(query_data)
        params.update(path_data)
        if body_data:
            params['data'] = body_data

        try:
            response, status_code, headers = self.method(**params)
        except Exception as e:
            body = getattr(e, 'body', None)
            status = getattr(e, 'status', None)
            if body and status:
                raise ApiException(json.loads(body), status)
            else:
                raise e

        response_data = None
        if response is not None:
            if isinstance(response, list):
                response_data = [i.to_dict() for i in response]
            else:
                response_data = response.to_dict()
        return ApiMethodResponse(
            response_data,
            self.response_serializer(status_code),
            status_code
        )

    def get_all_results(self, **kwargs):
        items = []
        res = {
            'count': None
        }
        offset = 0
        while res['count'] is None or res['count'] > len(items):
            res = self.call(offset=offset, **kwargs).data
            items += res['results']
            offset += len(res['results'])
        return items


class ApiModule:

    def __init__(self, module_name, module):
        self._module_name = module_name
        self._module = module
        self._serializers = Serializers(self.yaml['definitions'])
        self._set_methods()

    @property
    def yaml(self):
        return self._module.yaml

    @property
    def serializers(self):
        return self._serializers

    def _set_methods(self):
        for api, methods in self.yaml['paths'].items():
            api_name = (
                api.split("/")[1]
                .replace("-", " ")
                .title()
                .replace(" ", "")
            )
            api_class = getattr(self._module, "%sApi" % api_name)()

            for method, params in methods.items():
                if isinstance(params, dict):
                    method_name = params['operationId'].replace("-", "_")
                    setattr(self, method_name,
                            ApiMethod(
                                self._module_name,
                                getattr(
                                    api_class, "%s_with_http_info" % method_name),
                                params,
                                self.serializers
                            )
                            )
