from rest_framework.exceptions import APIException

class ApiException(APIException):
    def __init__(self, detail, code):
        self.status_code = code
        self.detail = detail