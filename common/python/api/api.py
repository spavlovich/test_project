from .config import API_CONFIG
from .api_module import ApiModule
import importlib

class _API:
    def __init__(self):
        self._available_modules = []
        self._set_modules()

    @property
    def available_modules(self):
        return self._available_modules
    
    def set_module(self, name, module):
        setattr(self, name, ApiModule(name, module))
        self._available_modules.append(getattr(self, name))

    def _set_modules(self):
        for api_name, host in API_CONFIG.items():
            try:
                api_module = importlib.import_module(api_name)
            except:
                continue

            api_configuration_module = api_module.configuration
            conf = api_configuration_module.Configuration()
            conf.host = host
            api_configuration_module.Configuration.set_default(conf)
            self.set_module(api_name, api_module)


API = _API()