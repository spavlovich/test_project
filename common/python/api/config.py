import os
ENV = os.environ.get('ENV')
_API_CONFIG = {
    'personnel_data': {
        'dev': 'http://personnel-data-python',
        'stage': 'http://personnel-data-web'
    },
    'person_data': {
        'dev': 'http://person-data-python',
        'stage': 'http://person-data-web'
    },
    'driver_data': {
        'dev': 'http://driver-data-python',
        'stage': 'http://driver-data-web'
    },
    'car_data': {
        'dev': 'http://car-data-python',
        'stage': 'http://car-data-web'
    },
    'company_data': {
        'dev': 'http://company-data-python',
        'stage': 'http://company-data-web'
    },
    'location_data': {
        'dev': 'http://location-data-python',
        'stage': 'http://location-data-web'
    },
    'customer_data': {
        'dev': 'http://customer-data-python',
        'stage': 'http://customer-data-web'
    },
    'order_data': {
        'dev': 'http://order-data-python',
        'stage': 'http://order-data-web'
    }
}

API_CONFIG = {}
for key, value in _API_CONFIG.items():
    API_CONFIG[key] = value[ENV]
