from model_utils import Choices

# Тип задач
TYPE_CMD = Choices(
    (1, 'SSH', 'ssh'),
    (2, 'MYSQL', 'mysql'),
    (3, 'RRD', 'rrd'),
    (4, 'SELENIUM', 'selenium'),
    
)

# Тип статуса задач
TYPE_JOB_STATUS = Choices(
    (True, 'START', 'start'),
    (False, 'STOP', 'stop'),
)

DICTIONARIES_CMDTYPE_SSH = 1


# default rabbitmq exange
DEFAULT_RABBITMQ_EXCHANGE = 'amq.topic'

# job rabbitmq
JOB_RABBITMQ = {
    'exchange': DEFAULT_RABBITMQ_EXCHANGE,
    'routing_key': 'job'
}

# job rabbitmq
JOB_SSH_RABBITMQ = {
    'exchange': DEFAULT_RABBITMQ_EXCHANGE,
    'routing_key': 'job_ssh'
}



# ==============
# Название очередей в RabbitMQ
# ==============

# Отправка водителем действий к заказу
# Перечень действий смотри в DRIVER_ACTIONS
# DRIVER_ACTION_RABBITMQ_QUEUE_NAME = 'driver_action'

# Когда сотрудник становится на смену и уходит со смены
# WORKTIME_RABBITMQ_QUEUE_NAME = 'worktime'

# Изменение CallSignState в Car Data(состояние, координаты позывного)
# CALLSIGNSTATE_RABBITMQ_QUEUE_NAME = 'callsign_state'


# ==============
# Название/маски ключей в Redis
# ==============

# Состояние позывного(автомобиль, водитель, статус позывного, координаты и т.д.)
# CALLSIGNSTATE_REDIS_MASK = 'callsign_state_%s'


# Перечень действий, которые может отправлять водитель к заказу
# DRIVER_ACTIONS = Choices(
#     (1, 'TAKE_ORDER', 'Беру заказ'),
#     (2, 'IN_PLACE', 'На месте'),
#     (3, 'DOING', 'Выполняю'),
#     (4, 'FULFILLED', 'Выполнен'),
# )

# Перечень статусов водителей
# DRIVER_STATUS = Choices(
#     (1, 'WORKING', 'Рабочий'),
#     (2, 'BLOCKED', 'Заблокирован'),
# )

# PASSENGER_ACTION = Choices(
#     (8, 'CANCEL', 'Отменен'),
# )

# LOCATION_NAME_POI = Choices(
#     (2, 'POI', 'Poi'),
# )
# LOCATION_NAME_STREET = Choices(
#     (1, 'STREET', 'Улица'),
# )

# LOCATION_NAME_TYPES_FOR_SEARCH = (
#     LOCATION_NAME_STREET +
#     LOCATION_NAME_POI
# )

# LOCATION_NAME_TYPES_FOR_ROUTE_POINT = (
#     LOCATION_NAME_POI +
#     Choices(
#             (3, 'BUILDING', 'Дом'),
#             (4, 'FRONTDOOR', 'Подъезд'),
#         )
# )

# Перечень статусов для позывных
# CALLSIGN_ACTION = Choices(
#     (1, 'FREE', 'Свободен'),
#     (2, 'RECEIVE', 'Получил заказ'),
#     (3, 'FULFILLS', 'Выполняет заказ'),
#     (4, 'BREAK', 'На перерыве'),
#     (5, 'FULFILLS_TAXIMETR', 'Выполняет заказ таксометр'),
#     (6, 'HOME_FREE', 'Домой свободен'),
#     (7, 'HOME_FULFILLS', 'Домой выполняет заказ'),
#     (8, 'LOCKED', 'Заблокирован'),
# )

# Типы телефонов
# TYPE_PHONE = Choices(
#     (1, 'MOB', 'Мобильный'),
#     (2, 'FIX', 'Фиксированный'),
# )

# Типы клиентов
# TYPE_CUSTOMER = Choices(
#     (1, 'PERSON', 'Частное лицо'),
#     (2, 'COMPANY', 'Компания'),
# )
