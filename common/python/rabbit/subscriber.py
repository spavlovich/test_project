from django.conf import settings
from django.db import connection
from pika.exceptions import ChannelClosedByBroker
from .rabbit import Rabbit
import time
import traceback
import logging
import threading

logger = logging.getLogger('MQSubscriber')

class Subscription:
    def __init__(self, config, *callbacks):
        self._config = config
        self._callbacks = callbacks

    @property
    def config(self):
        return self._config

    @property
    def callbacks(self):
        return self._callbacks
    
    

class Subscriber:

    def __init__(self, **kwargs):
        self.subscriptions = []

    def subscribe(self, config, *callbacks):
        subscription = Subscription(config, *callbacks)
        self.subscriptions.append(subscription)

    def connect(self, queue_name=None, auto_ack=True):
        configs = [i.config for i in self.subscriptions]
        try:
            r = Rabbit()
            r.subscribe(configs, self.callback, auto_ack, queue_name)
        except Exception as ex:

            if type(ex) == ChannelClosedByBroker and ex.args and ex.args[0] == 404:
                raise ex

            logger.error('Connect refused. Trying reconnect\n%s' %
                         traceback.format_exc())

            time.sleep(5)
            self.connect(queue_name, auto_ack)

    def callback_funcs(self, funcs, ch, method, properties, body):
        
        res = None
        for func in funcs:
            try:
                res = func(body, res)
            except Exception as e:
                res = None
                logging.fatal(e, exc_info=True)
                traceback.print_exc()

        connection.close()


    def callback(self, ch, method, properties, body):
        funcs = []
        for subscription in self.subscriptions:
            if (subscription.config['exchange'] == method.exchange
                            and subscription.config['routing_key'] == method.routing_key):
                funcs = subscription.callbacks
                break

        thread = threading.Thread(target=self.callback_funcs, args=(funcs, ch, method, properties, body))
        thread.start()