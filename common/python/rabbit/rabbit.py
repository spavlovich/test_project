from django.conf import settings
from django.core.serializers.json import DjangoJSONEncoder
import pika
import json

class Rabbit(object):

    def __init__(self, host=None, port=None, user=None, password=None):
        self.host = host or settings.RABBIT.get('host', 'localhost')
        self.port = port or settings.RABBIT.get('port', '5672')
        self.user = user or settings.RABBIT.get('user')
        self.password = password or settings.RABBIT.get('password')

        self.connection = None
        self.channel = None

        self.connect()

    def __del__(self):
        self.disconnect()


    def connect(self):
        if self.connection:
            return

        params = [
            self.host,
            self.port,
            '/'
        ]
        if self.user:
            params.append(pika.PlainCredentials(self.user, self.password or ""))
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(*params))
        self.channel = self.connection.channel()


    def disconnect(self):
        if self.connection:
            self.connection.close()
            self.connection = None

    """
    config - dict:
    {
        'exchange': string,
        'routing_key': 'string'
    }
    """
    def send(self, config, message):

        if type(message) != type(""):
            message = json.dumps(message,
                ensure_ascii=False,
                cls=DjangoJSONEncoder)

        self.channel.basic_publish(**config,
                      body=message)


    def subscribe_queue(self, queue, callback, auto_ack=False):
        def _callback(ch, method, properties, body):
            try:
                body = json.loads(body.decode("utf-8"))
            except:
                pass
            callback(ch, method, properties, body)

        self.channel.basic_consume(
                    queue,
                    _callback,
                    auto_ack=auto_ack
                )
        self.channel.start_consuming()
        

    """
    config - dict or list of dicts:
    {
        'exchange': string,
        'routing_key': 'string'
    }
    """
    def subscribe(self, configs, callback, auto_ack=False, queue_name=None):
        if queue_name:
            self.channel.queue_declare(queue_name, passive=True)
        else:
            result = self.channel.queue_declare('', exclusive=True)
            queue_name = result.method.queue

        if type(configs) not in (list, tuple):
            configs = [configs,]
        for config in configs:
            self.channel.queue_bind(**config, queue=queue_name)
            
        self.subscribe_queue(queue_name, callback, auto_ack)