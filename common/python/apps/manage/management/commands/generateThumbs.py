from django.core.management.base import BaseCommand
from common.django.fields import ThumbField
import django.apps


class Command(BaseCommand):

    def handle(self, *args, **options):
        # Берем все модели
        models = django.apps.apps.get_models()
        for model in models:
            thumb_fields = []
            # Проходимся по всем филдам модели
            for field in model._meta.fields:
                # Если филд это ThumbField
                if isinstance(field, ThumbField):
                    thumb_fields.append(field.name)
            # Обновляем ресайзы
            self.update_thumbs(model, thumb_fields)


    def update_thumbs(self, model, field_names):
        if not field_names:
            return

        # Берем все записи с модели
        items = model.objects.all()
        for item in items:
            # Проходимся по всем филдам ThumbField
            for field_name in field_names:
                thumb_field = getattr(item, field_name)
                if thumb_field:
                    # обновляем ресайзы
                    thumb_field.make_thumbs()
