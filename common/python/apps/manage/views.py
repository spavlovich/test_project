import os
import shutil
from django.apps import apps
from django.conf import settings
from django.core import management
from django.db import connection
from rest_framework.response import Response
from rest_framework import views
from rest_framework import status as status_code


FOUT = {'data': 'data.json', 'auth': 'auth.json'}
APP_DJ_AUTH = ['auth.user']
APP_DJ_EXCLUDE = [
    'contenttypes.contenttype',
    'admin.logentry',
    'auth.permission',
    'auth.group',
    'auth.user',
    'sessions.session',
    'admin.logentry'
]


def clear_dir_contains(dir_name):
    os.makedirs(dir_name, exist_ok=True)
    for name in os.listdir(dir_name):
        path_name = os.path.join(dir_name, name)
        if os.path.isfile(path_name):
            os.unlink(path_name)
        if os.path.isdir(path_name):
            shutil.rmtree(path_name)


def copy_dir_contains(from_dir, to_dir):
    os.makedirs(from_dir, exist_ok=True)
    os.makedirs(to_dir, exist_ok=True)
    for name in os.listdir(from_dir):
        from_path_name = os.path.join(from_dir, name)
        to_path_name = os.path.join(to_dir, name)
        if os.path.isfile(from_path_name):
            shutil.copyfile(from_path_name, to_path_name)
        if os.path.isdir(from_path_name):
            shutil.copytree(from_path_name, to_path_name)


def change_permissions_recursive(path, mode):
    for root, dirs, files in os.walk(path, topdown=False):
        for dir in [os.path.join(root, d) for d in dirs]:
            os.chmod(dir, mode)
    for file in [os.path.join(root, f) for f in files]:
        os.chmod(file, mode)
    os.chmod(path, mode)


class ManageData:

    def __init__(self, dir_media=None, dir_fixtures=None, fname='exclude.txt'):
        self.dir_base = settings.BASE_DIR
        self.dir_fixtures = '%s/%s' % (
            self.dir_base,
            'fixtures' if dir_fixtures is None else dir_fixtures
        )
        self.dir_media = '%s/%s' % (
            self.dir_fixtures,
            'media' if dir_media is None else dir_media
        )
        self.app_exclude = self.load_file(fname)
        self.app_list = [model._meta.__str__()
                         for model in apps.get_models() if model._meta.managed]

    def load_file(self, fname):
        res = []
        full_path = "%s/%s" % (self.dir_fixtures, fname)

        if os.path.exists(full_path) and os.path.getsize(full_path) > 0:
            with open(full_path, "r") as f:
                res = [l for l in f.read().splitlines() if l.rstrip()]
        return res

    def dumpdata(self, arr=None, dst=None):
        if arr is not None:
            # dump all data
            management.call_command(
                'dumpdata',
                '--indent=4',
                '--natural-foreign',
                '--natural-primary',
                *arr,
                output='%s/%s' % (self.dir_fixtures, dst),
                verbosity=0)
        else:
            raise Exception("no table names available")
        if dst is None:
            raise Exception("no destination path for fixtures")
        else:
            pass

    def loaddata(self, dst=None):
        if dst is not None:
            management.call_command(
                'loaddata',
                "%s/%s" % (self.dir_fixtures, dst),
                verbosity=0
            )
        else:
            raise Exception("Error: need path to load fixtures")

    def resetdata(self):
        for app in list(
                set(self.app_list) - set(self.app_exclude + APP_DJ_EXCLUDE)
        ):
                cursor = connection.cursor()
                cursor.execute("TRUNCATE %s CASCADE;" % app.replace(".", "_"))
        clear_dir_contains(settings.MEDIA_ROOT)
        

    def action(self, action):
        if action == 'create':
            self.create()
        elif action == 'load':
            self.load()
        elif action == 'reset':
            self.reset()

    def create(self):
        # create fixtures dir if not available
        if not os.path.exists(self.dir_fixtures):
            os.mkdir(self.dir_fixtures)

        # if exists MEDIA_ROOT copy data to media_root
        if not os.path.exists(self.dir_media):
            os.mkdir(self.dir_media)
            change_permissions_recursive(self.dir_media, 0o777)
        else:
            clear_dir_contains(self.dir_media)
        if os.path.exists(settings.MEDIA_ROOT):
            copy_dir_contains(settings.MEDIA_ROOT, self.dir_media)

        # create fixtures auth if available
        if 'django.contrib.auth' in settings.INSTALLED_APPS:
            self.dumpdata(APP_DJ_AUTH, FOUT['auth'])

        if settings.DATABASES:
            # create fixtures for application
            self.dumpdata(
                # exclude list = all apps - (exclude.txt + dj exclude)
                list(
                    set(self.app_list) - set(self.app_exclude + APP_DJ_EXCLUDE)
                ),
                FOUT['data']
            )

    def load(self):
        if not os.path.exists(settings.MEDIA_ROOT):
            os.mkdir(settings.MEDIA_ROOT)
            change_permissions_recursive(settings.MEDIA_ROOT, 0o777)
        else:
            clear_dir_contains(settings.MEDIA_ROOT)
        if os.path.exists(self.dir_media):
            copy_dir_contains(self.dir_media, settings.MEDIA_ROOT)

        # check auth
        if 'django.contrib.auth' in settings.INSTALLED_APPS:
            self.loaddata(FOUT['auth'])

        # check database available
        if settings.DATABASES:
            self.loaddata(FOUT['data'])

    def reset(self):
        self.resetdata()


class ResetViewSet(views.APIView):

    def get(self, request):
        try:
            ManageData().action("reset")
        except Exception as e:
            payload = {"msg": str(e)}
            return Response(payload, status=500)

        payload = {"msg": "data erase successfully"}
        return Response(payload, status=status_code.HTTP_200_OK)


class CreateDataViewSet(views.APIView):

    def get(self, request):
        try:
            ManageData().action("create")
        except Exception as e:
            payload = {"msg": str(e)}
            return Response(payload, status=500)

        payload = {"msg": "data create successfully"}
        return Response(payload, status=status_code.HTTP_200_OK)


class LoadDataViewSet(views.APIView):

    def get(self, request):
        try:
            ManageData().action("load")
        except Exception as e:
            payload = {"msg": str(e)}
            return Response(payload, status=500)

        payload = {"msg": "data loaded successfully"}
        return Response(payload, status=status_code.HTTP_200_OK)
