from django.urls import path
from .views import ResetViewSet, CreateDataViewSet, LoadDataViewSet


urlpatterns = [
    path(r'data-reset/', ResetViewSet.as_view(), name='reset'),
    path(r'data-save/', CreateDataViewSet.as_view(), name='create_data'),
    path(r'data-load/', LoadDataViewSet.as_view(), name='load_data'),
]
