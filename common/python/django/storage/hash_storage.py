from django.core.files.storage import FileSystemStorage
from django.core.files import File, locks
from django.core.files.move import file_move_safe
from django.utils.encoding import filepath_to_uri
from django.utils._os import safe_join
from django.conf import settings
from urllib.parse import urljoin
from hashfs import HashFS
import os

class HashStorage(FileSystemStorage):
    OS_OPEN_FLAGS = os.O_WRONLY | os.O_CREAT | os.O_EXCL | getattr(os, 'O_BINARY', 0)
    HashConfig = {
        'depth': 16,
        'width': 2,
        'algorithm': 'md5'
    }
    

    def save(self, name, content, max_length=None):
        if name is None:
            name = content.name

        if not hasattr(content, 'chunks'):
            content = File(content, name)

        dir_name, file_name = os.path.split(name)
        file_root, file_ext = os.path.splitext(file_name)
        fs = HashFS(os.path.join(settings.MEDIA_ROOT, dir_name), **self.HashConfig)
        address = fs.put(content, file_ext)

        name = os.path.join(dir_name, address.id)
        name = name + file_ext

        self._save(name, content)
        return name

    def path(self, name):
        dir_name, file_name = os.path.split(name)
        file_root, file_ext = os.path.splitext(file_name)

        fs = HashFS(os.path.join(settings.MEDIA_ROOT, dir_name), **self.HashConfig)
        idpath = fs.idpath(file_root, file_ext)
        address = fs.get(idpath)
        name = os.path.join(dir_name, address.relpath)

        return safe_join(self.location, name)

    def url(self, name):
        if self.base_url is None:
            raise ValueError("This file is not accessible via a URL.")
        
        url = filepath_to_uri(name)

        dir_name, file_name = os.path.split(url)
        file_root, file_ext = os.path.splitext(file_name)
        fs = HashFS(os.path.join(settings.MEDIA_ROOT, dir_name), **self.HashConfig)
        idpath = fs.idpath(file_root, file_ext)
        address = fs.get(idpath)
        url = os.path.join(dir_name, address.relpath)

        if url is not None:
            url = url.lstrip('/')
        return urljoin(self.base_url, url)


    def _save(self, name, content):
        full_path = self.path(name)

        # Create any intermediate directories that do not exist.
        directory = os.path.dirname(full_path)
        try:
            if self.directory_permissions_mode is not None:
                # os.makedirs applies the global umask, so we reset it,
                # for consistency with file_permissions_mode behavior.
                old_umask = os.umask(0)
                try:
                    os.makedirs(directory, self.directory_permissions_mode, exist_ok=True)
                finally:
                    os.umask(old_umask)
            else:
                os.makedirs(directory, exist_ok=True)
        except FileExistsError:
            raise FileExistsError('%s exists and is not a directory.' % directory)

        # There's a potential race condition between get_available_name and
        # saving the file; it's possible that two threads might return the
        # same name, at which point all sorts of fun happens. So we need to
        # try to create the file, but if it already exists we have to go back
        # to get_available_name() and try again.

        try:
            # This file has a file path that we can move.
            if hasattr(content, 'temporary_file_path'):
                file_move_safe(content.temporary_file_path(), full_path)

            # This is a normal uploadedfile that we can stream.
            else:
                # The current umask value is masked out by os.open!
                fd = os.open(full_path, self.OS_OPEN_FLAGS, 0o666)
                _file = None
                try:
                    locks.lock(fd, locks.LOCK_EX)
                    for chunk in content.chunks():
                        if _file is None:
                            mode = 'wb' if isinstance(chunk, bytes) else 'wt'
                            _file = os.fdopen(fd, mode)
                        _file.write(chunk)
                finally:
                    locks.unlock(fd)
                    if _file is not None:
                        _file.close()
                    else:
                        os.close(fd)
        except FileExistsError:
            pass

        if self.file_permissions_mode is not None:
            os.chmod(full_path, self.file_permissions_mode)

        # Store filenames with forward slashes, even on Windows.
        return name.replace('\\', '/')