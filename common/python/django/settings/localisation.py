import sys
globals().update(vars(sys.modules['project.settings']))

import locale

ugettext = lambda s: s
LANGUAGES = (
    ('uk', ugettext('Ukraine')),
    ('ru', ugettext('Russian')),
    ('en', ugettext('English')),
)

system_locale = locale.getdefaultlocale()[0]
if system_locale:
	system_locale = system_locale.split('_')[0]
if system_locale not in [i[0] for i in LANGUAGES]:
    system_locale = 'uk'
LANGUAGE_CODE = system_locale

TIME_ZONE = 'Europe/Kiev'
USE_I18N = True
USE_L10N = True
USE_TZ = True

MIDDLEWARE += [
    'common.django.middleware.locale.LocaleMiddleware',
]