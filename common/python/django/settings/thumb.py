import sys
globals().update(vars(sys.modules['project.settings']))
import os

THUMB_DEFAULT_SIZE = (1080, 1080)
THUMB_PATH = os.path.join(MEDIA_ROOT, 'thumb')
THUMB_SIZES = {
        's': { 'size': (80, 80),     'crop': True   },
        'm': { 'size': (235, 235),   'crop': True   },
        'l': { 'size': (345, 345),   'crop': False  }
    }