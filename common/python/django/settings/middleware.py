import sys
globals().update(vars(sys.modules['project.settings']))

MIDDLEWARE = []

if DEBUG:
    MIDDLEWARE += [
        'django.middleware.security.SecurityMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
    ]

    if 'DATABASES' in globals():
        MIDDLEWARE += [
            'django.contrib.sessions.middleware.SessionMiddleware',
            'django.middleware.csrf.CsrfViewMiddleware',
            'django.contrib.auth.middleware.AuthenticationMiddleware',
            'django.contrib.messages.middleware.MessageMiddleware',
        ]