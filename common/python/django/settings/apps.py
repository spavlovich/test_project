import sys
globals().update(vars(sys.modules['project.settings']))

INSTALLED_APPS = [
    'django_filters',
    'rest_framework_filters',
    'rest_framework',
    'corsheaders',
]

if 'DATABASES' in globals():
    INSTALLED_APPS += [
        'django.contrib.contenttypes',
    ]

if DEBUG:
    INSTALLED_APPS += [
        'django.contrib.staticfiles',
        'drf_yasg',
    ]

    if 'DATABASES' in globals():
        INSTALLED_APPS += [
            'django.contrib.admin',
            'django.contrib.auth',
            'django.contrib.sessions',
            'django.contrib.messages',
        ]