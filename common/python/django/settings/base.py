import os

BASE_DIR = os.path.abspath(__file__)
BASE_DIR = os.path.dirname(BASE_DIR)
BASE_DIR = os.path.dirname(BASE_DIR)
BASE_DIR = os.path.dirname(BASE_DIR)
BASE_DIR = os.path.dirname(BASE_DIR)

SECRET_KEY = '207p722dreag@to!rrisniyhstyrx3jgi9+5b9tf-#k22vmv82'

# SECURITY WARNING: don't run with debug turned on in production!
ENV = os.environ.get('ENV')
if ENV in ('dev', 'stage'):
    DEBUG = True
else:
    DEBUG = False

ALLOWED_HOSTS = ["*"]
ROOT_URLCONF = 'project.urls'
WSGI_APPLICATION = 'project.wsgi.application'

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
DEFAULT_FILE_STORAGE = 'common.django.storage.hash_storage.HashStorage'

INSTALLED_APPS = []
MIDDLEWARE = []