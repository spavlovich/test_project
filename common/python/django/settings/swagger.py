import sys
globals().update(vars(sys.modules['project.settings']))

from drf_yasg import openapi
import os

SWAGGER_SETTINGS = {
    "DEFAULT_AUTO_SCHEMA_CLASS": 'common.rest_framework.swagger_auto_schema.SwaggerAutoSchema',
    "DEFAULT_INFO": openapi.Info(
            title="%s API" % os.environ.get('SERVICE_NAME', "").title(),
            default_version='v1'
        ),
    'DEFAULT_PAGINATOR_INSPECTORS': [
        'common.rest_framework.inspectors.DjangoRestResponsePagination',
        'drf_yasg.inspectors.CoreAPICompatInspector',
    ],
}