import sys
globals().update(vars(sys.modules['project.settings']))

CORS_ORIGIN_ALLOW_ALL = True
MIDDLEWARE += [
    'corsheaders.middleware.CorsMiddleware',
]