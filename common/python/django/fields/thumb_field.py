from django.db.models import ImageField
from django.db.models.fields.files import ImageFieldFile
from django.conf import settings
from django.core.files import File
import PIL
import os
import io

# Вписывает картинку в прямоугольник size
def resize(input_image, size):
    image = input_image.copy()
    if image.size > size: # don't stretch smaller images
        image.thumbnail(size, PIL.Image.ANTIALIAS)
    return image

# Делает ресайз и кроп по-центру согласно размерам size
def crop_resize(input_image, size):
    image = input_image.copy()
    # crop center
    w, h = image.size
    if w > h: # width is larger then necessary
        x, y = (w - h) // 2, 0
        h += (w - h) % 2
    else: # height >= width (height is larger)
        x, y = 0, (h - w) // 2
        w += (h - w) % 2
    image = image.crop((x, y, w - x, h - y))
    return resize(image, size)


class ThumbFieldFile(ImageFieldFile):
    
    @property
    def thumb_path_template(self):
        dir_name, file_name = os.path.split(self.path)
        file_root, file_ext = os.path.splitext(file_name)
        path_template = dir_name.replace(settings.MEDIA_ROOT, settings.THUMB_PATH)
        path_template = os.path.join(path_template, file_root, '%%s%s' % file_ext)
        return path_template

    @property
    def thumb_url_template(self):
        url_template = self.thumb_path_template
        url_template = url_template.replace(settings.MEDIA_ROOT + '/', '')
        url_template = os.path.join(settings.MEDIA_URL, url_template)
        return url_template

    def make_thumbs(self):
        thumb_dir_name, template_file_name = os.path.split(self.thumb_path_template)
        # Создаем папку куда будем складывать ресайзы
        os.makedirs(thumb_dir_name, exist_ok=True)

        image = PIL.Image.open(self.path)
        # Проходимся по всем необходимым ресайзам, которые описаны в конфиге settings.THUMB_SIZES
        for key, value in settings.THUMB_SIZES.items():
            resize_path = self.thumb_path_template % key
            if value.get('crop'):
                # Ресайзим и кропаем
                resize_image = crop_resize(image, value['size'])
            else:
                # Просто ресайзим
                resize_image = resize(image, value['size'])
            # Сохраняем ресайз
            resize_image.save(resize_path, format='JPEG', quality=80, optimize=True)

    def save(self, name, content, save=True):
        image = PIL.Image.open(content)
        image.thumbnail(settings.THUMB_DEFAULT_SIZE, PIL.Image.ANTIALIAS)
        image = image.convert('RGB')
        byte = io.BytesIO()
        image.save(fp=byte, format='JPEG', quality=80, optimize=True)
        content = File(byte, content.name)

        super().save(name, content, save)
        # Создаем ресайзы
        self.make_thumbs()

class ThumbField(ImageField):
    attr_class = ThumbFieldFile

    def __init__(self, verbose_name=None, name=None, width_field=None, height_field=None, upload_to='image/', **kwargs):
      super().__init__(verbose_name, name, width_field, height_field, upload_to=upload_to, **kwargs)


    