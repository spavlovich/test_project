from django.conf import settings
from django.urls import re_path
from drf_yasg.views import get_schema_view

schema_view = get_schema_view(
    settings.SWAGGER_SETTINGS["DEFAULT_INFO"],
    public=True,
)
urlpatterns = [
    re_path(r'^swagger(?P<format>\.json|\.yaml)$',
            schema_view.without_ui(cache_timeout=0), name='schema-json'),
    re_path(r'^$', schema_view.with_ui('swagger', cache_timeout=0),
            name='schema-swagger-ui'),
    re_path(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0),
            name='schema-redoc'),
]