from django.conf import settings
from django.utils import translation
from django.http import HttpResponse
from django.middleware.locale import LocaleMiddleware as DjangoLocaleMiddleware
from rest_framework import serializers, status
import json


class LangSerializer(serializers.Serializer):
    lang = serializers.ChoiceField(
            required = False,
            default = '',
            choices = settings.LANGUAGES,
            help_text = "Язык"
        )


class LocaleMiddleware(DjangoLocaleMiddleware):

    def process_request(self, request):
        super().process_request(request)

        data = {}
        if request.GET.get('lang'):
            data['lang'] = request.GET.get('lang')
        lang_serializer = LangSerializer(data=data)
        lang_serializer.is_valid(raise_exception=True)
        language = lang_serializer.validated_data.get('lang')
        if language:
            translation.activate(language)
            request.LANGUAGE_CODE = translation.get_language()
