from drf_yasg.utils import swagger_auto_schema
from common.rest_framework.viewsets import ApiViewSet
from common.api import API


class LocationSearchViewSet(ApiViewSet):
    pagination_class = None
    serializer_classes = {
        'list': API.location_data.serializers.Search,
    }

    @swagger_auto_schema(query_serializer=API.location_data.search_list.serializers.query)
    def list(self, request):
        res = API.location_data.search_list.call(request=request)
        return res.response()
