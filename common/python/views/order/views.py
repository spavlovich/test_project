from django.utils import translation
from drf_yasg.utils import swagger_auto_schema
from rest_framework.response import Response
from common.constants import LOCATION_NAME_TYPES_FOR_ROUTE_POINT
from common.rest_framework.viewsets import ApiViewSet
from common.django.middleware.locale import LangSerializer
from .serializers import OrderSerializer
from common.api import API
import json


class GenericCommonOrderViewSet(ApiViewSet):
    serializer_classes = {
        'list': OrderSerializer,
        'retrieve': OrderSerializer,
        'create': OrderSerializer,
        'update': OrderSerializer,
    }

    def set_route_point_names(self, orders):
        lang = translation.get_language()
        rout_points = []

        for order in orders:
            rout_points += order['routepoints']

        names = {}
        for route_point in rout_points:
            address_id = route_point['address_id']
            address_type = route_point['address_type']

            if not address_id:
                continue

            if not names.get(address_type):
                names[address_type] = {}
            names[address_type][address_id] = None

        lang_kwarg = {}
        if lang:
            lang_kwarg['lang'] = lang

        for address_type, items in names.items():
            ids = ",".join([str(i) for i in items.keys()])
            if address_type == LOCATION_NAME_TYPES_FOR_ROUTE_POINT.POI:
                pois = API.location_data.poi_list.get_all_results(
                        id__in=ids,
                        **lang_kwarg
                    )
                for poi in pois:
                    name = poi['name']
                    if poi['address']:
                        name = "%s (%s, %s)" % (
                            name,
                            poi['address']['street']['name'],
                            poi['address']['building']['name']
                        )
                        address = poi['address']
                    else:
                        address = {}
                    address['poi'] = {
                        'id': poi['id'],
                        'name': poi['name']
                    }
                    items[poi['id']] = {
                        'name': name,
                        'address': address,
                        'lat': poi['lat'],
                        'lng': poi['lng'],
                    }
            elif address_type == LOCATION_NAME_TYPES_FOR_ROUTE_POINT.BUILDING:
                buildings = API.location_data.building_list.get_all_results(
                        id__in=ids,
                        **lang_kwarg
                    )
                for building in buildings:
                    name = "%s, %s" % (
                        building['address']['street']['name'],
                        building['name']
                    )
                    building['address']['building'] = {
                        'id': building['id'],
                        'name': building['name']
                    }
                    items[building['id']] = {
                        'name': name,
                        'address': building['address'],
                        'lat': building['lat'],
                        'lng': building['lng'],
                    }
            elif address_type == LOCATION_NAME_TYPES_FOR_ROUTE_POINT.FRONTDOOR:
                frontdoors = API.location_data.frontdoor_list.get_all_results(
                        id__in=ids,
                        **lang_kwarg
                    )
                for frontdoor in frontdoors:
                    name = "%s, %s, %s" % (
                        frontdoor['address']['street']['name'],
                        frontdoor['address']['building']['name'],
                        frontdoor['name']
                    )
                    frontdoor['address']['frontdoor'] = {
                        'id': frontdoor['id'],
                        'name': frontdoor['name']
                    }
                    items[frontdoor['id']] = {
                        'name': name,
                        'address': frontdoor['address'],
                        'lat': frontdoor['lat'],
                        'lng': frontdoor['lng'],
                    }

        for route_point in rout_points:
            address_id = route_point['address_id']
            address_type = route_point['address_type']
            name_data = names.get(address_type, {}).get(address_id) or {}
            for key in name_data.keys():
                route_point[key] = name_data[key]

class CommonOrderListViewSet(GenericCommonOrderViewSet):
    @swagger_auto_schema(query_serializer=LangSerializer)
    def list(self, request, params={}):
        payload = {
                    'limit': self.paginator.get_limit(request),
                    'offset': self.paginator.get_offset(request)
                }
        payload.update(params)
        data = API.order_data.order_full_list.call(**payload).data
        self.set_route_point_names(data['results'])
        return self.get_paginated_response(data)

class CommonOrderRetrieveViewSet(GenericCommonOrderViewSet):
    @swagger_auto_schema(query_serializer=LangSerializer)
    def retrieve(self, request, pk):
        data = API.order_data.order_full_read.call(id=pk).data
        data_list = [data, ]
        self.set_route_point_names(data_list)
        serializer = OrderSerializer(data_list[0])
        return Response(serializer.data)

class CommonOrderGenericUpdateViewSet(GenericCommonOrderViewSet):
    def _update(self, request, pk=None):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if pk:
            item = API.order_data.order_full_update.call(id=pk, data=serializer.data).data
        else:
            item = API.order_data.order_full_create.call(data=serializer.data).data

        return self.retrieve(request, item['id'])

class CommonOrderUpdateViewSet(CommonOrderGenericUpdateViewSet):
    def update(self, request, pk=None):
        return self._update(request, pk)


class CommonOrderCreateViewSet(CommonOrderGenericUpdateViewSet):
    def create(self, request):
        return self._update(request)


class CommonOrderViewSet(
            CommonOrderListViewSet,
            CommonOrderRetrieveViewSet,
            CommonOrderUpdateViewSet,
            CommonOrderCreateViewSet
        ):
    pass