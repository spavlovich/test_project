from rest_framework import serializers
from common.constants import LOCATION_NAME_TYPES_FOR_ROUTE_POINT
from common.api import API


class RoutePointAddressNameSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()


class RoutePointAddressSerializer(serializers.Serializer):
    city = RoutePointAddressNameSerializer(
            required=False
        )
    street = RoutePointAddressNameSerializer(
            required=False
        )
    building = RoutePointAddressNameSerializer(
            required=False
        )
    frontdoor = RoutePointAddressNameSerializer(
            required=False
        )
    poi = RoutePointAddressNameSerializer(
            required=False
        )


class RoutePointSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    address_type = serializers.ChoiceField(
            choices=LOCATION_NAME_TYPES_FOR_ROUTE_POINT,
            required=False,
            allow_null=True,
            label="Тип объекта",
            help_text="\n".join(["%s: %s" % i for i in LOCATION_NAME_TYPES_FOR_ROUTE_POINT])
        )
    address_id = serializers.IntegerField(
            required=False,
            allow_null=True,
            label="ID объекта"
        )
    lat = serializers.FloatField(
            label="Широта"
        )
    lng = serializers.FloatField(
            label="Долгота"
        )

    name = serializers.CharField(
            required=False,
            allow_null=True,
            label="Название",
            help_text="Полное текстовое представление точки маршрута. Например: полный адрес дома"
        )
    address = RoutePointAddressSerializer(
            required=False,
            allow_null=True,
            help_text="Адрес"
        )

    
class OrderSerializer(API.order_data.serializers.OrderFull):
    routepoints = RoutePointSerializer(many=True, help_text="Точка маршрута")