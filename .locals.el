; create push-data & add data
(setq push-data (make-hash-table))
(puthash :prj-sync nil push-data)
(puthash :prj-name "" push-data)
(puthash :prj-dir-remote "/home/user/media/source_project/dj" push-data)
(puthash :prj-dir-local "/home/user/media/source_project/dj" push-data)
(puthash :ssh-user (gethash :user (gethash :linode_vds my-data)) push-data)
(puthash :ssh-ip (gethash :ip (gethash :linode_vds my-data)) push-data)
(puthash :ssh-pwd (gethash :pwd (gethash :linode_vds my-data)) push-data)
(puthash :ssh-port (gethash :port (gethash :linode_vds my-data)) push-data)
(puthash :ssh-key (gethash :key (gethash :linode_vds my-data)) push-data)
(puthash :ssh-key-pwd (gethash :pwd_pwd (gethash :linode_vds my-data)) push-data)
(puthash :ssh-key-pwd-prompt (gethash :key_pwd_prompt (gethash :linode_vds my-data)) push-data)
(puthash :cur-path (expand-file-name default-directory) push-data)
(gethash :prj-name push-data)
(Hlpr-push-file push-data)

; run project.py
(global-set-key (kbd "C-M-r") (lambda ()
                                (interactive)
                                (save-buffer)
                                (cd (find-dot-env))
                                ; (async-shell-command "./project.py")

                                (setq default-directory (concat (find-dot-env) "/.."))
                                (async-shell-command "./project.py")

                                ; (async-shell-command (concat (find-dot-env) "/../project.py"))
                                (other-window 1)
                                (delete-other-windows)
                                (local-set-key (kbd "q") 'kill-this-buffer)))

; autosync when save
(defun prj-sync ()
  (Hlpr-push-file push-data))
(add-hook 'after-save-hook 'prj-sync)

; ?
(defun execvp (&rest args)
  "Simulate C's execvp() function.
  Quote each argument seperately, join with spaces and call shell-command-to-string to run in a shell."
  (let ((cmd (mapconcat 'shell-quote-argument args " ")))
    (shell-command-to-string cmd)))


;; projectile ignore dir, file, extention
(setq projectile-globally-ignored-directories
      (append '("dbdata"
                ;; "common"
                )
              projectile-globally-ignored-directories))

;; (setq projectile-globally-ignored-files
;;       (append '(;; emacs files
;;                 ".projectile"
;;                 ".locals.el"
;;                 ;; workspace files
;;                 ".workspace.sh"
;;                 ".env"
;;                 "manage.sh"
;;                 ;; git files
;;                 ".gitignore"
;;                 ".gitmodules"
;;                 ;; django files
;;                 "__init__.py"
;;                 "manage.py"
;;                 ;; "wsgi.py"
;;                 ;; project files
;;                 "docker-compose.dev.yml"
;;                 "docker-compose.yml"
;;                 "docker-compose.stage.yml"
;;                 ".gitlab-ci.yml"
;;                 "client.yaml"
;;                 ;; other files
;;                 "README.md")
;;               projectile-globally-ignored-files))

;; (setq projectile-globally-ignored-file-suffixes
;;       (append '(".orig"
;;                 ".origin"
;;                 ".svg"
;;                 ".pyc")
;;               projectile-globally-ignored-file-suffixes))

;; ag settings
(setq ag-grep-ignored
      (append '(
                ;; "--ignore 'common'"
                "--ignore 'packages/ycmd'"
                "--ignore 'node_modules'"
                ;; ignore extentions
                "--ignore ''*.pyc'"
                "--ignore ''*.orig'"
                ;; ignore files
                "--ignore 'client.yaml'")))
(remo:set:helm-grep-ag-command ag-grep-ignored)

;; projectile fine files settings
(setq ag-ff-ignored
      (append '(
                ;; "--ignore 'common'"
                "--ignore 'packages/ycmd'"
                "--ignore 'node_modules'"
                "--ignore 'docker'"
                "--ignore 'migrations'"
                "--ignore 'app/fixtures'"
                ;; ignore files
                "--ignore 'app/project/wsgi.py'"
                "--ignore 'app/manage.py'"
                "--ignore 'manage.sh'"
                "--ignore '__init__.py'"
                "--ignore '.workspace.sh'"
                "--ignore '.locals.el'"
                "--ignore '.gitignore'"
                "--ignore '.gitmodules'"
                "--ignore '.hgignore'"
                "--ignore '.projectile'"
                "--ignore '.gitlab-ci.yml'"
                "--ignore 'docker-compose.yml'"
                "--ignore 'docker-compose.dev.yml'"
                "--ignore 'docker-compose.stage.yml'"
                "--ignore 'client.yaml'")))
(remo:set:projectile-generic-command ag-ff-ignored)
(remo:set:projectile-git-command ag-ff-ignored)
(remo:set:projectile-hg-command ag-ff-ignored)

; "ag . --hidden -l -0 --nocolor --nogroup --ignore 'dbdata' --ignore '.env' --ignore '.hg' --ignore '.git' --ignore 'node_modules' --ignore '.gitmodules'"
; "ag . --hidden -l -0 --nocolor --nogroup --ignore '.env' --ignore '.hg' --ignore '.git' --ignore 'common' --ignore 'packages/ycmd' --ignore 'node_modules' --ignore 'client.yaml'"



;; ----------------------------------------------------------------------------
; (global-set-key (kbd "<f5>") (lambda ()
;                                 (interactive)
;                                                               (save-buffer)
;                                                               (async-shell-command "./project.py")
;                                 (other-window 1)
;                                 (delete-other-windows)
;                                 ))
;                                                                               ; (message "Run deploy")
; ;;                                                            (insert (shell-command-to-string (concat"./script.py " "deploy:project.com")))))
; ;;(shell-command-to-string "./script.py deploy:project.com")))

; (local-set-key [f5] (lambda ()
;                       (interactive)
;                       (message "Run deploy")))
