from django.contrib import admin
from .models import (
    ApiName,
    AppName,
)

admin.site.register(ApiName)
admin.site.register(AppName)

