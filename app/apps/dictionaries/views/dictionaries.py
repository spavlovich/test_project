from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets, mixins
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from django.core.exceptions import ObjectDoesNotExist


from ..models import (
    ApiName,
    AppName,
)
from ..serializers.dictionaries import (
    ApiSerializer,
    AppNameSerializer,
    AppNameQuerySerializer,
)


class ApiViewSet(mixins.ListModelMixin,
                 mixins.CreateModelMixin,
                 mixins.DestroyModelMixin,
                 viewsets.GenericViewSet):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = ApiName.objects.all()
    serializer_class = ApiSerializer


class AppNameViewSet(mixins.ListModelMixin,
                     mixins.CreateModelMixin,
                     mixins.DestroyModelMixin,
                     viewsets.GenericViewSet):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    queryset = AppName.objects.all()
    serializer_class = AppNameSerializer
    filter_fields = {
        'name': ['exact', 'in', ],
        'api__name': ['exact', ],
    }


class AppNameTestViewSet(viewsets.GenericViewSet):
    pagination_class = None

    @swagger_auto_schema(query_serializer=AppNameQuerySerializer)
    @action(detail=False)
    def test(self, request):
        query_serializer = AppNameQuerySerializer(data=request.GET)
        query_serializer.is_valid(raise_exception=True)
        query_data = query_serializer.validated_data
        search = query_data.get('search')
        try:
            q = AppName.objects.\
                values().\
                filter(api__name=search)
        except ObjectDoesNotExist:
            return Response(status=404)
        return Response(q)
