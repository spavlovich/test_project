from rest_framework import serializers
from ..models import (
    ApiName,
    AppName,
)


class ApiSerializer(serializers.ModelSerializer):

    class Meta:
        model = ApiName
        fields = '__all__'


class AppNameSerializer(serializers.ModelSerializer):

    class Meta:
        model = AppName
        fields = '__all__'


class AppNameQuerySerializer(serializers.Serializer):
    search = serializers.CharField(
        required=False,
        help_text="Строка поиска по ключу API"
    )
