from django.urls import path, include
from rest_framework import routers
from .views.dictionaries import (
    ApiViewSet,
    AppNameViewSet,
)

router = routers.DefaultRouter()
router.register('api', ApiViewSet, basename="appname")
router.register('appname', AppNameViewSet)


urlpatterns = [
    path('', include(router.urls)),
]
