# Generated by Django 2.2 on 2020-01-14 11:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dictionaries', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Zzz',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Имя приложения')),
                ('api_name', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='times', to='dictionaries.Api')),
            ],
            options={
                'verbose_name': 'App name',
                'verbose_name_plural': "App name's",
            },
        ),
    ]
