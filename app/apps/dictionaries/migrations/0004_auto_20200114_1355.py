# Generated by Django 2.2 on 2020-01-14 11:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dictionaries', '0003_auto_20200114_1354'),
    ]

    operations = [
        migrations.RenameField(
            model_name='appname',
            old_name='api_name',
            new_name='api',
        ),
        migrations.AlterField(
            model_name='api',
            name='name',
            field=models.CharField(max_length=100, verbose_name='Имя Api'),
        ),
    ]
