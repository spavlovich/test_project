from django.db import models


class ApiName(models.Model):
    name = models.CharField(max_length=100, verbose_name='Имя Api')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Api'
        verbose_name_plural = 'Api\'s'


class AppName(models.Model):
    name = models.CharField(max_length=100, verbose_name='Имя приложения')
    api = models.ForeignKey(
        ApiName,
        on_delete=models.CASCADE,
        related_name='api',
        # null=True
    )

    def __str__(self):
        return "name=%s | api=%s" % (self.name, self.api)

    class Meta:
        verbose_name = 'App name'
        verbose_name_plural = 'App name\'s'
