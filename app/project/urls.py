from django.conf import settings
from django.urls import include, path
from rest_framework import routers
from django.urls import path, include
from django.conf.urls import url
from apps.dictionaries.views import dictionaries

router = routers.DefaultRouter()
router.register(r'api', dictionaries.AppNameTestViewSet, basename="api_test")

urlpatterns = [
    path('dictionaries/', include('apps.dictionaries.urls')),
    path('manage/', include('common.apps.manage.urls')),
    path('', include(router.urls)),

]

if settings.DEBUG is True:
    urlpatterns = [
        path('', include('common.django.urls.debug_toolbar')),
        path('', include('common.django.urls.swagger')),
        path('', include('common.django.urls.media')),
        path('', include('common.django.urls.admin')),
    ] + urlpatterns
