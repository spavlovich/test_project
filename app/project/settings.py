from common.django.settings.base import *
from common.django.settings.db import *
from common.django.settings.apps import *
from common.django.settings.middleware import *
from common.django.settings.templates import *
from common.django.settings.auth import *
from common.django.settings.localisation import *
from common.django.settings.rest_framework import *
from common.django.settings.swagger import *
from common.django.settings.corsheaders import *
from common.django.settings.debug_toolbar import *
from common.django.settings.thumb import *
from common.django.settings.rabbit import *

INSTALLED_APPS += [
     'apps.dictionaries',
    'common.apps.manage',
]

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://redis/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}
