#!/bin/bash

. ./common/bash/manage-scripts/__scripts__.sh
actions="build-dev \
    stop-all \
    destroy-all \
    collectstatic \
    makemigrations \
    migrate \
    runserver \
    superuser-create \
    manage \
    db-connect \
    uni-test"

# -----
. ./common/bash/manage.sh
