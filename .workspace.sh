#!/bin/bash

set -a
. ./.env
set +a

#DOCKER
CONT_SRV_PY="python"
CONT_DB_TYPE="postgres"
# posgresql
DB_PG_HOST='db'
DB_PG_NAME='django'
DB_PG_PORT=5432
# mysql
DB_MYSQL_HOST="mysql"
DB_MYSQL_USER="root"
DB_MYSQL_PWD="password"
DB_MYSQL_PORT=3306
DB_MYSQL_NAME="django"
# tmux
SESSION=${CONTAINER}
SESSION_TYPE="docker" # or mysql
# path's
PATH_LOCAL="$( cd "$(dirname "$0")" ; pwd -P )"
PATH_REMOTE="/var/www/${SESSION}"
# ssh
SSH_HOST="127.0.0.1"
SSH_USER="user"
SSH_PWD="zaq12wsx"
SSH_PORT="22222"
# django
DJ_RUNSERVER_HOST="0.0.0.0"
DJ_RUNSERVER_PORT=80
# profile(remote == 1, local == 0)
PROFILE=0

# set window title
printf "\033k$(echo ${SESSION}| tr '[:lower:]' '[:upper:]')\033\\"

# set remote profile
if [ "${1}" == "remote" ]; then
	PROFILE=1
fi

function Start() {
	if [[ ! -z $(tmux ls |grep "${SESSION}:") ]]; then
		tmux a -t ${SESSION}
	else
		# set up tmux
		tmux start-server

		# -------------------------------------------------------------------------
		# CREATE dev panel
		# -------------------------------------------------------------------------
		tmux new-session -d -s $SESSION -n dev #"vim -S ~/.vim/sessions/kittybusiness"

		# dev --> 1st window(vim)
		tmux selectp -t 1
		tmux send-keys "cd ${PATH_LOCAL}/app;clear" C-m

		# dev -->2st window(manage.py)
		tmux split-window -t :.1
		tmux swap-pane -s :.1 -t :.2
		tmux select-layout main-vertical
		HEIGHT=$(($(tmux display-message -p '#{pane_height}')))
		tmux resize-pane -t :.1 -x $(( ${HEIGHT} * 2 ))
		if [ ${PROFILE} -eq 1 ];then
			if [[ ${SESSION_TYPE} == "docker" ]]; then
				# tmux send-keys "docker-compose exec ${CONT_SRV_PY} ./manage.py runserver ${DJ_RUNSERVER_HOST}:${DJ_RUNSERVER_PORT}" C-m
                tmux send-keys "docker exec -it ${PYTHON_CONTAINER} bash -c \"for i in {1..100}; do ./manage.py runserver ${DJ_RUNSERVER_HOST}:${DJ_RUNSERVER_PORT};sleep 10;done\"" C-m
			else
				# with ssh pwd
				tmux send-keys "sshpass -p ${SSH_PWD} ssh -oStrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -p ${SSH_PORT} -t -l ${SSH_USER} ${SSH_HOST} 'cd ${PATH_REMOTE};clear;./manage.py runserver ${DJ_RUNSERVER_HOST}:${DJ_RUNSERVER_PORT}'" C-m
				# # with ssh key
				# tmux send-keys "ssh -oStrictHostKeyChecking=no -t -l ${SSH_USER} ${SSH_HOST} 'cd ${PATH_REMOTE}/${SESSION};clear;./manage.py runserver 0.0.0.0:${DJ_RUNSERVER_PORT}'" C-m
			fi
		else
			tmux send-keys "cd ${PATH_LOCAL}/${SESSION};clear;./manage.py runserver 0.0.0.0:${DJ_RUNSERVER_PORT}" C-m
		fi

		# dev --> 3rd window(terminal)
		tmux split-window -t :.1
		tmux swap-pane -s :.1 -t :.2
		tmux select-layout main-vertical
		HEIGHT=$(($(tmux display-message -p '#{pane_height}')))
		tmux resize-pane -t :.1 -x $(( ${HEIGHT} * 2 ))
		if [ ${PROFILE} -eq 1 ];then
			if [[ ${SESSION_TYPE} == "docker" ]]; then
				tmux send-keys "docker-compose exec ${CONT_SRV_PY} bash -c 'clear;/bin/bash -i'" C-m
			else
				# with ssh pwd
				tmux send-keys "sshpass -p '${SSH_PWD}' ssh -oStrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -p ${SSH_PORT} -t -l ${SSH_USER} ${SSH_HOST} 'cd ${PATH_REMOTE};clear;/bin/bash'" C-m
				# # with ssh key
				# tmux send-keys "ssh -oStrictHostKeyChecking=no -t -l ${SSH_USER} ${SSH_HOST} 'cd ${PATH_REMOTE}/${SESSION};clear;bash -i'" C-m
			fi
		else
			tmux send-keys "cd ${PATH_LOCAL}/${SESSION};clear" C-m
		fi

		# swap 3 window to 1
		tmux select-pane -t 3
		tmux swap-pane -s :. -t :.1 \; select-pane -t :.1


		# -------------------------------------------------------------------------
		# CREATE django panel
		# -------------------------------------------------------------------------
		tmux new-window -t ${SESSION}:2 -n django

		# django --> 2nd window(django shell)
		tmux selectp -t 2
		if [ ${PROFILE} -eq 1 ];then
			if [[ ${SESSION_TYPE} == "docker" ]]; then
				tmux send-keys "docker-compose exec ${CONT_SRV_PY} bash -c 'clear;./manage.py shell'" C-m
			else
				# with ssh pwd
				tmux send-keys "sshpass -p ${SSH_PWD} ssh -oStrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -t -l ${SSH_USER} ${SSH_HOST} -p ${SSH_PORT} 'cd ${PATH_REMOTE};clear;./manage.py shell'" C-m
				# # with ssh key
				# tmux send-keys "ssh -oStrictHostKeyChecking=no -t -l ${SSH_USER} ${SSH_HOST} -p ${SSH_PORT} 'cd ${PATH_REMOTE}/${SESSION};clear;./manage.py shell;bash -i'" C-m
			fi
		else
			tmux send-keys "cd ${PATH_LOCAL}/${SESSION};clear;./manage.py shell" C-m
		fi


		# -------------------------------------------------------------------------
		# CREATE db panel
		# -------------------------------------------------------------------------
		# create panel3 db panel
		tmux new-window -t ${SESSION}:3 -n db

		# db --> 2nd window(mysql console)
		tmux selectp -t 2
		if [ ${PROFILE} -eq 1 ];then
			if [[ ${SESSION_TYPE} == "docker" ]]; then
				if [[ ${CONT_DB_TYPE} == "postgres" ]]; then
					tmux send-keys "docker-compose exec ${DB_PG_HOST} bash -c 'psql -h ${DB_PG_HOST} ${DB_PG_NAME}'" C-m
				elif [[ ${CONT_DB_TYPE} == "mysql" ]]; then
					tmux send-keys "docker-compose exec ${CONT_SRV_PY} bash -c 'mysql -u ${DB_MYSQL_USER} -p${DB_MYSQL_PWD} -h ${DB_MYSQL_HOST} -P ${DB_MYSQL_PORT} ${DB_MYSQL_NAME};/bin/bash -i'" C-m
				fi
			else
				# with ssh pwd
				tmux send-keys "sshpass -p ${SSH_PWD} ssh -oStrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -t -l ${SSH_USER} ${SSH_HOST} -p ${SSH_PORT} 'mysql -u ${DB_MYSQL_USER} -p${DB_MYSQL_PWD} -h ${DB_MYSQL_HOST} -P ${DB_MYSQL_PORT} ${DB_MYSQL_NAME};/bin/bash -i'" C-m
				# # with ssh key
				# tmux send-keys "ssh -oStrictHostKeyChecking=no -t -l ${SSH_USER} ${SSH_HOST} -p ${SSH_PORT} 'mysql -u ${DB_MYSQL_USER} -p${DB_MYSQL_PWD} ${DB_MYSQL_NAME};/bin/bash -i'" C-m
			fi
		else
			tmux send-keys "mysql -u ${DB_MYSQL_USER} -p${DB_MYSQL_PWD} ${DB_MYSQL_NAME}" C-m
		fi


		# return to main vim window
		tmux select-window -t ${SESSION}:1

		# Finished setup, attach to the tmux SESSION!
		tmux attach-session -t $SESSION
	fi
}

function Local() {
	Start
}
function Remote() {
	Start
}

# ** START HERE **
case "$1" in
	'local')
		Local "$1"
		;;
	'remote')
		Remote "$1"
		;;
	*)
		echo "Usage for $AGENT_NAME: $0 { local | remote }"
		exit 1
		;;
esac









# # Select pane 1, set dir to api, run vim
# tmux selectp -t 1 
# tmux send-keys "cd $dj/${SESSION};vim" C-m
#
# # Split pane 1 horizontal by 60%, start manage.py
# # tmux splitw -h -p 30
# tmux send-keys "cd $dj/${SESSION}/${SESSION};./manage.py runserver 0.0.0.0:${DJ_RUNSERVER_PORT}" C-m
# tmux send-keys "cd $dj/${SESSION}/${SESSION};ls -a" C-m
#
# # Select pane 2 
# tmux selectp -t 2
# # Split pane 2 vertiacally by 25%
# tmux splitw -v -p 75
#
# # select pane 3, set to api root
# tmux selectp -t 3
# tmux send-keys "api" C-m 
#
# # Select pane 1
# tmux selectp -t 1
#
# # # create a new window called scratch
# # tmux new-window -t $SESSION:1 -n scratch
#
# # return to main vim window
# tmux select-window -t $SESSION:0

# # Create new pane in current directory
# tmux split-window -t :.0 -c "#{pane_current_path}"
# tmux swap-pane -s :.0 -t :.1
# tmux select-layout main-vertical
# tmux run "tmux resize-pane -t :.0 -x \"$(echo \"#{window_width}/2/1\" | bc)\""

# Create new pane
# tmux split-window -t :.0
# tmux swap-pane -s :.0 -t :.1
# tmux select-layout main-vertical
# HEIGHT=$(($(tmux display-message -p '#{pane_height}')))
# tmux resize-pane -t :.0 -x $(( ${HEIGHT} * 2 ))
# tmux send-keys "cd $dj/${SESSION}/${SESSION};./manage.py runserver 0.0.0.0:${DJ_RUNSERVER_PORT}" C-m

