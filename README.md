Задача: реализовать API, позволяющее добавлять, изменять,
просматривать и удалять данные в модели "Приложения".  "Приложения" –
это модель, которая хранит в себе внешние источники, которые будут
обращаться к API. Обязательные поля модели: ID, Название приложения,
Ключ API. Поле "Ключ API" нельзя менять через API напрямую, должен
быть метод, позволяющий создать новый ключ API.  После добавления
приложения – должна быть возможность использовать "Ключ API"
созданного приложения для осуществления запросов к метод /api/test,
метод должен возвращать JSON, содержащий всю информацию о приложении.

Использовать следующие технологии: Django 2.2.7, Django REST framework.

Результат выполнения задачи:
- исходный код приложения в github
- инструкция по разворачиванию приложения (в docker или локально)
- инструкция по работе с запросами к API: как авторизоваться, как добавить, как удалить и т.д.
------------------------------------------------------------------------------------------------------------------------------------------

Результат выполненного тестового задания:
Установка:

1. Клонируем репозиторий: git clone git@bitbucket.org:spavlovich/test_project.git
2. Заходим в директорию test_project
3. Выполняем в консоли команду
```
./manage.sh build-dev
```
4. Запускаем django debug server в контейнере docker:
```
docker exec -it test_project-python bash -c "for i in {1..100}; do ./manage.py runserver 0.0.0.0:80;sleep 10;done"
```
5. Сервис доступен по адресу
```
http://127.0.0.1:9999
```
6. Логин\пароль: administrator:zaq12wsx

# Загружаем данные с fixture
curl -X GET "http://127.0.0.1:9999/manage/data-load/" -H "accept: application/json"

Манипулирование данными:
CRUD api
http://127.0.0.1:9999/dictionaries/api/
# Получить все api
```
curl -X GET "http://127.0.0.1:9999/dictionaries/api/" -H "accept: application/json" --user administrator:zaq12wsx
```

# Добавить api
```
curl -X POST "http://127.0.0.1:9999/dictionaries/api/" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"name\": \"string\"}" --user administrator:zaq12wsx
```

# Удалить api
```
curl -X DELETE "http://127.0.0.1:9999/dictionaries/api/29/" -H "accept: application/json" --user administrator:zaq12wsx
```


CRUD appname
http://127.0.0.1:9999/dictionaries/appname/
# Получить все appname
```
curl -X GET "http://127.0.0.1:9999/dictionaries/appname/" -H "accept: application/json" --user administrator:zaq12wsx
```

# Добавить appname
```
curl -X POST "http://127.0.0.1:9999/dictionaries/appname/" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"name\": \"string\", \"api\": 1}" --user administrator:zaq12wsx
```

# Удалить appname
```
curl -X DELETE "http://127.0.0.1:9999/dictionaries/appname/1/" -H "accept: application/json" --user administrator:zaq12wsx
```
